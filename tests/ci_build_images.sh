#!/usr/bin/env bash

declare -r SELF_DIR="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)"

(
    cd "$SELF_DIR/.." || exit
    ./ci_build_image.sh
)

if docker compose pull; then
    echo "Images pulled successfully"
else
    ./build_images.sh && docker compose push
fi
