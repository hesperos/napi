#!/usr/bin/perl

use strict;
use warnings;
$|++;

use lib qw(./common/lib/perl5);
use GitInstaller;
use NetInstall;

GitInstaller::prepareGithubPkg("SimonKagstrom",
    "kcov",
    "43",
    \&NetInstall::cmakeInstall
);

__END__

