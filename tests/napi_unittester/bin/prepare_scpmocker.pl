#!/usr/bin/perl

use strict;
use warnings;
$|++;

use lib qw(./common/lib/perl5);
use GitInstaller;
use NetInstall;

GitInstaller::prepareGitlabPkg("hesperos",
    "-",
    "scpmocker",
    "0.4",
    \&NetInstall::pythonInstall
);

__END__
