#!/usr/bin/env bash

declare -r SELF_DIR="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)"

# build base image
(
    cd "${SELF_DIR}/.." || exit
    ./build_image.sh
)

# build test images
docker compose build "$@"
