#!/bin/bash

updateInstallation() {
    ./build_images.sh napi-integtester
}

usage() {
    echo "run_integration_tests.sh [-u]"
    echo "Options:"
    echo
    echo "  -u - update napi installation in container and run tests"
    echo "  -U - update napi installation in container and exit (doesn't run tests)"
    echo
}

while getopts "uUh" option; do
    case "$option" in
        u)
            updateInstallation
            ;;

        U)
            # only update, don't run tests
            updateInstallation
            exit 0
            ;;

        h)
            usage
            exit 0
            ;;

        *)
            echo "Unexpected argument" >/dev/stderr
            usage
            exit 1
            ;;
    esac
done

# run the tests
docker compose run \
    -T \
    --remove-orphans \
    --rm \
    napi-integtester \
    napi_integtester/bin/run_integration_tests.sh "$@"
