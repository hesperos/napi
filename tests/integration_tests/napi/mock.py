#!/usr/bin/env python3

import pytest_httpserver

from . import xml_result

class NapiprojektMock(object):
    DEFAULT_ADDRESS = 'localhost'
    DEFAULT_PORT = 1234

    def __init__(self,
                 logger,
                 address = DEFAULT_ADDRESS,
                 port = DEFAULT_PORT):
        self.address = address
        self.port = port
        self.logger = logger
        self.http = pytest_httpserver.HTTPServer(self.address, self.port)
        self.defaultContentType = 'text/xml; charset=UTF-8'

    def startServer(self):
        if not self.http.is_running():
            self.logger.info("Starting mock server")
            self.http.start()
        else:
            self.logger.info("Won't start mock server - it's already running")

    def stopServer(self):
        if self.http.is_running():
            self.logger.info("Stopping mock server")
            self.http.stop()
        else:
            self.logger.info("Mock server is not running")

    def getUrl(self):
        return self.http.url_for("/")

    def getRequest(self, n = 0):
        return self.getLog()[n]

    def getLog(self):
        return self.http.log

    def checkExpectations(self):
        self.http.check_assertions()

    def programPlainRequest(self,
            blob = None,
            status = None,
            times = 1):

        body = None
        data = blob.getData() if blob else ''
        if not status:
            status = 200 if blob else 404

        uri = "/unit_napisy/dl.php"
        while times > 0:
            self.http.expect_oneshot_request(uri,
                                     method="GET").respond_with_data(
                                             data,
                                             status=status,
                                             content_type=self.defaultContentType)
            times -= 1


    def programXmlRequest(self,
            media,
            subtitles = None,
            cover = None,
            movieDetails = None,
            times = 1):
        status = 200
        body = None
        uri = '/api/api-napiprojekt3.php'
        data = xml_result.XmlResult(subtitles, cover, movieDetails).toString()
        while times > 0:
            self.http.expect_oneshot_request(uri,
                                     method="POST").respond_with_data(
                                             data,
                                             status=status,
                                             content_type=self.defaultContentType)
            times -= 1
