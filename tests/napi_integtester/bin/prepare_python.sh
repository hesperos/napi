#!/bin/bash

python -m venv $NAPI_INTEGTESTER_SOURCE/venv
source $NAPI_INTEGTESTER_SOURCE/venv/bin/activate
pip install -r ./napi_integtester/requirements.txt
