#!/usr/bin/env bash

source "$NAPI_INTEGTESTER_SOURCE/venv/bin/activate"

if [ -n "$1" ]; then
    python -m unittest discover --local -vfs integration_tests -k "$1"
else
    python -m unittest discover --local -vfs integration_tests
fi
