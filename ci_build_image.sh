#!/usr/bin/env bash

declare -r REMOTE_IMAGE="${REGISTRY}/napi:${NAPI_IMAGE_VERSION}"
declare -r REMOTE_IMAGE_LATEST="${REGISTRY}/napi:latest"

declare -r LOCAL_IMAGE="napi:${NAPI_IMAGE_VERSION}"
declare -r LOCAL_IMAGE_LATEST="napi:latest"

if docker pull "$REMOTE_IMAGE" && docker pull "$REMOTE_IMAGE_LATEST"; then
    docker tag "$REMOTE_IMAGE" "$LOCAL_IMAGE"
    docker tag "$REMOTE_IMAGE_LATEST" "$LOCAL_IMAGE_LATEST"
else
    ./build_image.sh
    docker tag "$LOCAL_IMAGE" "$REMOTE_IMAGE"
    docker tag "$LOCAL_IMAGE_LATEST" "$REMOTE_IMAGE_LATEST"
    docker push "$REMOTE_IMAGE"
    docker push "$REMOTE_IMAGE_LATEST"
fi
