If you have any proposition regarding this script or You wish to report a bug,
Please, do so. Use the gitlab issue tracker available here:

[gitlab issues](https://gitlab.com/hesperos/napi/issues)

Or contact the author directly. Your contribution will help to make this tool
better and more suited to Your needs.
