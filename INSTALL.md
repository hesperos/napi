# Instalation

**bashnapi** is using *cmake* as a build tool.  Refer to README.md for more details.

## 3rd_party tools required

Use `test_tools.sh` script to check if your system has all required tools
available.

You'll need at least the following for `napi.sh` to function properly:

- bash shell
- wget (can be installed via Homebrew/Macports)
- find tool
- dd (coreutils)
- md5sum (in OS X, it's md5)
- cut
- mktemp
- 7z (if napiprojekt3 is used) (p7zip)
- awk (preferably GNU awk)
- grep
