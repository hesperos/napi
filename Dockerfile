FROM debian:12 AS builder

ENV SOURCE_DIR=/usr/src/napi

RUN apt-get update -y
RUN apt-get install -y \
        make \
        cmake

RUN mkdir -p $SOURCE_DIR
ADD . $SOURCE_DIR
WORKDIR $SOURCE_DIR

RUN mkdir -p build && \
        cd build && \
        cmake .. && \
        make && \
        make install


FROM debian:12 AS runner

ENV NAPI_HOME=/home/napi

RUN apt-get update -y
RUN apt-get install -y \
        ffmpeg \
        mediainfo

RUN apt-get install -y \
        wget \
        p7zip-full \
        gawk

RUN useradd -m -U napi -d $NAPI_HOME

COPY --from=builder /usr/local/bin/napi.sh /usr/local/bin
COPY --from=builder /usr/local/bin/subotage.sh /usr/local/bin
COPY --from=builder /usr/local/lib/napi /usr/local/lib/napi

USER napi
ENTRYPOINT ["napi.sh"]
