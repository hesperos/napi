#!/usr/bin/env bash

declare -r SELF_DIR="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)"

docker build -t "napi:${NAPI_IMAGE_VERSION}" "${SELF_DIR}"
docker tag "napi:${NAPI_IMAGE_VERSION}" napi:latest
